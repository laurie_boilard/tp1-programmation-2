class Formations{
    constructor(id, nom, desc, prix, cours){
        this.id = id;
        this.nom = nom;
        this.desc = desc; 
        this.prix = prix;
        this.cours = cours;
    }
}

const data = [   
    new Formations( 0, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "MongoDB"),
    new Formations( 1, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "NodeJS"),
    new Formations( 2, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "MongoDB"),
    new Formations( 3, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "NodeJS"),
    new Formations( 4, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "MongoDB"),
    new Formations( 5, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "NodeJS"),
    new Formations( 6, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "MongoDB"),
    new Formations( 7, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "NodeJS"),
    new Formations( 8, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "MongoDB"),
    new Formations( 9, "Formation ipsum", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua", "100 $", "NodeJS"),
];

exports.getFormationById = function(id){
    return data[id]
}

exports.getByType = function(type){
    var formation_by_type = [];
    for( let i = 0; i < data.length; i++){
        if(data[i].cours == type){
            formation_by_type.push(data[i])
        }
    };
    return formation_by_type
}

exports.addFormation = function(dat){
    let id = data.length;
    data.push(new Formations(id, dat.nom, dat.prenom, dat.courriel, dat.telephone, dat.sujet, dat.description))
    return true;
}

exports.allFormations = data;