class Formulaires{
    constructor(id, nom, prenom, courriel, telephone, sujet, desc){
        /*
        * id
        * nom
        * prenom
        * courriel
        * telephone
        * sujet de la demande => inscription ou demande
        * description
        */
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.courriel = courriel;
        this.telephone = telephone;
        this.sujet = sujet
        this.desc = desc; 
    }
}

const data = [   
    new Formulaires('', '', '', '', '', '', '')
];

exports.getFormulaireById = function(id){
    return data[id]
}

exports.addFormulaire = function(dat){
    let id = data.length;
    data.push(new Formulaires(id, dat.nom, dat.prenom, dat.courriel, dat.telephone, dat.sujet, dat.desc))
    return true;
} 


exports.allFormulaires = data; 