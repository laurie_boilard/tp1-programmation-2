var express = require('express');
var router = express.Router();

var les_formations = require('../costum_modules/formations.js')
var articles = require('../costum_modules/articles.js')
var les_formulaires = require('../costum_modules/formulaires.js')


/* GET index page = index.hbs */
router.get('/', function(req, res, next) {
  res.render('formations/index', { title: 'Accueil', texte: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat"});
});


/* GET formations.hbs */
router.get('/formations', function(req, res, next) {
  res.render('formations/formations', { title: 'Nos formations', formation1: "MongoDB", formation2: 'NodeJS'})
});


/* GET formations MongoDB page || NodeJS page */

/* Get formations for MongoDB.hbs page */
router.get('/formations/MongoDB', function(req, res, next){
  res.render('formations/MongoDB',{ title: 'Formations MongoDB', les_formations: les_formations.getByType('MongoDB')})
})

/* Get formations for NodeJS.hbs page */
router.get('/formations/NodeJS', function(req, res, next){
  res.render('formations/NodeJS',{ title: 'Formations NodeJS', les_formations: les_formations.getByType('NodeJS')})
})

/* Get formation FOR EACH MongoDB & NodeJS formations */
router.get('/formations/:id',function(req,res,next){
  res.render('formations/formation',{ formation: les_formations.getFormationById(req.params.id) })
})


/* GET blog.hbs */
router.get('/blog', function(req, res, next){
  res.render('blog',{ title: 'Le blog', intertitre: "Voici une liste d'articles", articles: articles.allArticles } )
})


/* GET articles for blog.hbs */
router.get('/articles/:id',function(req,res,next){
  res.render('articles',{ title: 'Les articles', intertitre: "Voici la liste d'articles détaillées", articles: articles.allArticles })
})


/* GET add.hbs */
router.get('/formulaire/contact', function(req,res,next){
  res.render('contact', { title: 'Page contact'});s
})

router.post('/formulaire/contact', function(req,res,next){
  let form = les_formulaires.addFormulaire(req.body)
  if(form){
    res.render('formulaire',{ title: 'Le formulaire a été envoyé', formulaires: les_formulaires.allFormulaires } )
  }
  console.log(req.body)
})

module.exports = router;

